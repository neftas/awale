$("document").ready(function() {
    AWALE.init();
});

var AWALE = (function($) {
    const MAKE_MOVE_URL = "/app/makeMove",
          RESET_URL = "/app/reset",
          DISABLED = "disabled",
          DISABLE = "disable",
          ENABLE = "enable",
          NORTH = "north",
          SOUTH = "south",
          UNCLICKABLE = "unclickable";
    var notification = null,
        bigPitLeft = null,
        bigPitRight = null,
        sortedPits = null,
        resetButton = null,
        connectButton = null,
        disconnectButton = null,
        stompClient = null;

    function init() {
        notification = $("#notification p");
        bigPitLeft = $("#bigPitLeft");
        bigPitRight = $("#bigPitRight");
        resetButton = $("#resetButton");
        connectButton = $("#connectButton");
        disconnectButton = $("#disconnectButton");
        sortedPits = $("[id^=smallPit]").sort(function(a, b) {
            var aNum = parseInt(/\d+$/.exec(a.id)[0]);
            var bNum = parseInt(/\d+$/.exec(b.id)[0]);
            if (aNum < bNum) {
                return -1;
            } else {
                return 1;
            }
        });
        // insert big pits
        sortedPits.splice(6, 0, bigPitRight);
        sortedPits.push(bigPitLeft);

        // TODO: disable fields of north initially?
        // toggleFields(NORTH, DISABLE);

        // TODO: disable connect initially?
        //

        resetButton.click(function(event) {
            event.preventDefault();
            sendReset();
        });

        connectButton.click(function(event) {
            event.preventDefault();
            connect();
        });

        disconnectButton.click(function(event) {
            event.preventDefault();
            disconnect();
        });

        $(document).on("click", "[id^=smallPit]:not(.unclickable)", function(event) {
            event.preventDefault();
            if (stompClient === null) {
                notification.text("First connect to server by clicking 'Connect'.");
                return;
            }
            sendMove(this);
        });
    }

    function sendMove(field) {
        stompClient.send(MAKE_MOVE_URL, {}, JSON.stringify({field: /\d+$/.exec(field.id)[0]}));
    }

    function sendReset() {
        stompClient.send(RESET_URL, {}, null);
    }

    function connect() {
        var socket = new SockJS("/awale-websocket");
        stompClient = Stomp.over(socket);
        stompClient.connect({}, function(frame) {
            setConnected(true);
            console.log("Connected: " + frame);
            stompClient.subscribe("/topic/gameState", function(gameState) {
                onSuccessfulMove(JSON.parse(gameState.body));
            });
        });
    }

    function setConnected(connected) {
        connectButton.prop(DISABLED, connected);
        disconnectButton.prop(DISABLED, !connected);
    }

    function disconnect() {
        if (stompClient !== null) {
            stompClient.disconnect();
        }
        stompClient = null;
        setConnected(false);
        console.log("Disconnected");
    }

    function onSuccessfulReset(response, status, jqXHR) {
        onSuccessfulMove(response, status, jqXHR);
    }

    function toggleFields(side, toggle) {
        side = side.toLowerCase();
        toggle = toggle.toLowerCase();
        if ([NORTH, SOUTH].indexOf(side) === -1) {
            console.log("parameter 'side' has invalid value '" + side + "'");
            return;
        }
        if ([ENABLE, DISABLE].indexOf(toggle) === -1) {
            console.log("parameter 'toggle' has invalid value '" + toggle + "'");
            return;
        }
        var pit = null;
        switch (side.toLowerCase()) {
            case NORTH:
                for (var i = 7, j = 13; i < j; i++) {
                    pit = $(sortedPits[i]);
                    toggle === ENABLE ? pit.removeClass(UNCLICKABLE) : pit.addClass(UNCLICKABLE);
                }
                break;
            case SOUTH:
                for (var i = 0, j = 6; i < j; i++) {
                    pit = $(sortedPits[i]);
                    toggle === ENABLE ? pit.removeClass(UNCLICKABLE) : pit.addClass(UNCLICKABLE);
                }
                break;
            default:
                // should never be reached
                break;
        }
    }

    function onSuccessfulMove(response, status, jqXHR) {
        console.log(response);
        console.log(status);
        console.log(jqXHR);
        var southPlayer = response.southPlayer;
        var northPlayer = response.northPlayer;
        var activePlayer = response.activePlayer;
        var winner = null;
        if (response.isGameFinished) {
            if (southPlayer.score === northPlayer.score) {
                notification.text("It's a tie!");
            } else {
                winner = southPlayer.score > northPlayer.score ? southPlayer : northPlayer;
                notification.text(winner.name + " has won with " + winner.score + " points! Congratulations!");
            }
        } else if (response.error) {
            switch(response.error) {
                case "INVALID_MOVE":
                    notification.text(activePlayer + " made an invalid move");
                    break;
                default:
                    notification.text("Some error occurred.");
                    break;
            }
        } else {
            notification.text("Now it's " + response.activePlayer.name + "'s turn.")
        }
        toggleFields(response.activePlayer.position, ENABLE);
        toggleFields(response.passivePlayer.position, DISABLE);
        for (var i = 0, j = sortedPits.length; i < j; i++) {
            $(sortedPits[i]).text(response.pits[i].stones.length);
        }
    }

    return { init: init }
}(jQuery));