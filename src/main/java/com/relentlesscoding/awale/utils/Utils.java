package com.relentlesscoding.awale.utils;

import java.util.UUID;

public class Utils {

    public static String createUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }
}
