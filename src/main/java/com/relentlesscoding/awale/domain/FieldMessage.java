package com.relentlesscoding.awale.domain;

public class FieldMessage {

    private String field;

    public FieldMessage() {
    }

    public FieldMessage(String field) {
        this.field = field;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }
}
