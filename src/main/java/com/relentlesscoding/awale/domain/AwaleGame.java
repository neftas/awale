package com.relentlesscoding.awale.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonValue;

public enum AwaleGame {
    INSTANCE;

    private Board board;
    private Player southPlayer;
    private Player northPlayer;
    private Player activePlayer;
    private Player passivePlayer;
    private boolean finished;

    AwaleGame() {
        this.board = Board.INSTANCE;
        init();
    }

    private void init() {
        this.southPlayer = Player.newInstance("Player1", PlayerPosition.SOUTH);
        this.northPlayer = Player.newInstance("Player2", PlayerPosition.NORTH);
        this.activePlayer = southPlayer;
        this.passivePlayer = northPlayer;
        this.finished = false;
    }

    /**
     * Resets game to initial condition.
     */
    public void reset() {
        this.board = Board.INSTANCE;
        init();
    }

    public Board getBoard() {
        return board;
    }

    public Player getSouthPlayer() {
        return southPlayer;
    }

    public Player getNorthPlayer() {
        return northPlayer;
    }

    public Player getActivePlayer() {
        return activePlayer;
    }

    public Player getPassivePlayer() {
        return passivePlayer;
    }

    public void setActivePlayer(final Player player) {
        this.activePlayer = player;
    }

    /**
     * Makes the active player the waiting ("passive") player and vice versa.
     */
    public void switchPlayers() {
        final Player oldActivePlayer = this.activePlayer;
        this.activePlayer = this.passivePlayer;
        this.passivePlayer = oldActivePlayer;
    }

    public boolean isFinished() {
        return finished;
    }

    public void becomeFinished() {
        this.finished = true;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AwaleGame{");
        sb.append("board=").append(board);
        sb.append(", activePlayer=").append(activePlayer);
        sb.append(", finished=").append(finished);
        sb.append('}');
        return sb.toString();
    }

    @JsonValue
    final AwaleGame value() {
        return this;
    }
}
