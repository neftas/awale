package com.relentlesscoding.awale.domain;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public enum Board {
    INSTANCE;

    private final Integer numPits = 14;
    @JsonInclude
    private List<Pit> pits;
    private final String[] pitIds = {"A", "B", "C", "D", "E", "F", "G", "a", "b", "c", "d", "e", "f", "g"};

    Board() {
        init();
    }

    private void init() {
        createPits();
        linkPits();
    }

    private void linkPits() {
        // link pits together
        for (int i = 0; i < numPits; i++) {
            Pit pit = pits.get(i);
            // (0 - 1) % 14 = -1
            if (i == 0) {
                pit.setPrevious(pits.get(numPits - 1));
            } else {
                pit.setPrevious(pits.get((i - 1) % numPits));
            }
            // (13 + 1) % 14 = 0
            pit.setNext(pits.get((i + 1) % numPits));
            if (i == 6) {
                pit.setOpposite(pits.get(13));
                continue;
            }
            if (i == 13) {
                pit.setOpposite(pits.get(6));
                continue;
            }
            pit.setOpposite(pits.get(numPits - 2 - i));
        }
    }

    private void createPits() {
        final Set<Integer> locationBigPits = new HashSet<>();
        locationBigPits.add(6);
        locationBigPits.add(13);
        this.pits = new ArrayList<>(this.numPits);

        for (int i = 0; i < this.numPits; i++) {
            // 2 big pits
            if (locationBigPits.contains(i)) {
                this.pits.add(new Pit(this.pitIds[i], true));
                continue;
            }
            // 12 small pits
            this.pits.add(new Pit(this.pitIds[i], false));
        }
    }

    public void reset() {
        init();
    }

    public List<Pit> getPits() {
        return this.pits;
    }

    public void setPits(List<Pit> pits) {
        this.pits = pits;
    }
}
