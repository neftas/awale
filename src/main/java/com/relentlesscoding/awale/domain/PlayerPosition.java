package com.relentlesscoding.awale.domain;

import java.util.ArrayList;
import java.util.List;

public enum PlayerPosition {
    NORTH("north"), SOUTH("south");

    private final static int NUM_PITS = 7;
    private final String position;
    private List<Pit> pits;
    private Pit bigPit;

    PlayerPosition(final String position) {
        this.position = position;
        init();
    }

    private void init() {
        List<Pit> boardPits = Board.INSTANCE.getPits();
        if ("south".equals(position)) {
            this.pits = boardPits.subList(0, 7);
        } else {
            this.pits = boardPits.subList(7, 14);
        }
        this.bigPit = this.pits.get(this.pits.size() - 1);
    }

    public List<Pit> getPits() {
        List<Pit> deepCopy = new ArrayList<>(NUM_PITS);
        for (Pit pit : pits) {
            deepCopy.add(pit.clone());
        }
        return deepCopy;
    }

    public Pit getBigPit() {
        return bigPit;
    }

    public void reset() {
        init();
    }

    @Override
    public String toString() {
        return "PlayerPosition{" +
                "position='" + position + '\'' +
                ", pits=" + pits +
                '}';
    }
}
