package com.relentlesscoding.awale.domain;

import com.relentlesscoding.awale.utils.Utils;

public class Player {
    private String id;
    private String name;
    private PlayerPosition position;
    private boolean connected;
    private String sessionId;

    private Player(final String id, final String name, final PlayerPosition position) {
        assert id != null : "'id' is null";
        assert !id.isEmpty() : "'id' is empty";
        assert name != null : "'name' is null";
        assert !name.isEmpty() : "'name' is empty";
        assert position != null : "'position' is null";

        this.id = id;
        this.name = name;
        this.position = position;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Player{");
        sb.append("id='").append(id).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", position=").append(position);
        sb.append('}');
        return sb.toString();
    }

    public static Player newInstance(final String name, final PlayerPosition position) {
        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException("'name' is null or the empty string");
        }
        if (position == null) {
            throw new IllegalArgumentException("'position' is null");
        }

        final String uuid = Utils.createUUID();
        return new Player(uuid, name, position);
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public PlayerPosition getPosition() {
        return position;
    }

    public int getScore() {
        return this.position.getBigPit().count();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Player player = (Player) o;

        return id.equals(player.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
}
