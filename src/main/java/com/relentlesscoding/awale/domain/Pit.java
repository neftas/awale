package com.relentlesscoding.awale.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;
import java.util.List;

public class Pit implements Cloneable {
    @JsonInclude
    private String id;
    @JsonInclude
    private boolean isBig = false;
    // TODO (neftas): num should come from properties
    public final static int NUM_INITIAL_STONES = 6;
    @JsonInclude
    private List<Stone> stones = new ArrayList<>();
    @JsonIgnore
    private Pit next;
    @JsonIgnore
    private Pit previous;
    @JsonIgnore
    private Pit opposite;

    Pit(final String id, final boolean isBig) {
        if (id == null || id.isEmpty()) {
            throw new IllegalArgumentException("'id' is null or the empty string");
        }

        this.id = id;
        this.isBig = isBig;

        if (!this.isBig) {
            for (int i = 0; i < NUM_INITIAL_STONES; i++) {
                stones.add(Stone.newInstance());
            }
        }
    }

    public Pit getNext() {
        return next;
    }

    void setNext(final Pit next) {
        this.next = next;
    }

    public Pit getPrevious() {
        return previous;
    }

    void setPrevious(final Pit previous) {
        this.previous = previous;
    }

    public Pit getOpposite() {
        return opposite;
    }

    public void setOpposite(Pit opposite) {
        this.opposite = opposite;
    }

    public String getId() {
        return id;
    }

    public boolean isBig() {
        return isBig;
    }

    public boolean isSmall() {
        return !isBig;
    }

    /**
     * Indicated whether this pit contains {@link Stone}s or not.
     * @return {@code true} if this pit does not contain any stones, {@code false} otherwise.
     */
    public boolean isEmpty() {
        return this.stones.isEmpty();
    }

    @JsonInclude
    public int count() {
        return this.stones.size();
    }

    public List<Stone> getStones() {
        List<Stone> deepCopy = new ArrayList<>(stones.size());
        for (Stone stone : stones) {
            deepCopy.add(stone.clone());
        }
        return deepCopy;
    }

    public void setStones(List<Stone> stones) {
        this.stones = new ArrayList<>(stones.size());
        for (Stone stone : stones) {
            this.stones.add(stone.clone());
        }
    }

    public void emptyPit() {
        this.stones.clear();
    }

    public void addStone(final Stone stone) {
        if (stone == null) {
            throw new NullPointerException("'stone' is null");
        }

        this.stones.add(stone);
    }

    @Override
    public Pit clone() {
        try {
            return (Pit) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();  // should never happen
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Pit pit = (Pit) o;

        return id.equals(pit.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Pit{");
        sb.append("id='").append(id).append('\'');
        sb.append(", isBig=").append(isBig);
        sb.append(", stones=").append(stones);
        sb.append(", count=").append(stones.size());
        sb.append('}');
        return sb.toString();
    }

}
