package com.relentlesscoding.awale.domain;

import java.util.List;

public class AwaleGameState {
    private List<Pit> pits;
    private Player northPlayer;
    private Player southPlayer;
    private Player activePlayer;
    private Player passivePlayer;
    private boolean isGameFinished;
    private String error;

    public AwaleGameState() {}

    public AwaleGameState(List<Pit> pits,
                          Player northPlayer,
                          Player southPlayer,
                          Player activePlayer,
                          Player passivePlayer,
                          boolean isGameFinished,
                          String error)
    {
        this.pits = pits;
        this.northPlayer = northPlayer;
        this.southPlayer = southPlayer;
        this.activePlayer = activePlayer;
        this.passivePlayer = passivePlayer;
        this.isGameFinished = isGameFinished;
        this.error = error;
    }

    public List<Pit> getPits() {
        return pits;
    }

    public void setPits(List<Pit> pits) {
        this.pits = pits;
    }

    public Player getNorthPlayer() {
        return northPlayer;
    }

    public void setNorthPlayer(Player northPlayer) {
        this.northPlayer = northPlayer;
    }

    public Player getSouthPlayer() {
        return southPlayer;
    }

    public void setSouthPlayer(Player southPlayer) {
        this.southPlayer = southPlayer;
    }

    public Player getActivePlayer() {
        return activePlayer;
    }

    public void setActivePlayer(Player activePlayer) {
        this.activePlayer = activePlayer;
    }

    public Player getPassivePlayer() {
        return passivePlayer;
    }

    public void setPassivePlayer(Player passivePlayer) {
        this.passivePlayer = passivePlayer;
    }

    public boolean isGameFinished() {
        return isGameFinished;
    }

    public void setGameFinished(boolean gameFinished) {
        isGameFinished = gameFinished;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
