package com.relentlesscoding.awale.domain;

import com.relentlesscoding.awale.utils.Utils;

public class Stone implements Cloneable {
    private String id;

    private Stone(final String id) {
        assert id != null : "'id' is null";
        assert !id.isEmpty() : "'id' is empty";

        this.id = id;
    }

    public static Stone newInstance() {
        final String uuid = Utils.createUUID();
        return new Stone(uuid);
    }

    @Override
    public Stone clone() {
        try {
            return (Stone) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();  // can't happen
        }
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Stone{" +
                "id='" + id + '\'' +
                '}';
    }
}
