package com.relentlesscoding.awale.services;

import com.relentlesscoding.awale.domain.AwaleGame;
import com.relentlesscoding.awale.domain.Pit;
import com.relentlesscoding.awale.domain.Player;
import com.relentlesscoding.awale.domain.Stone;

import java.util.List;

public class PlayerService {

    public static boolean isValidMove(final Player player, final Pit pit) {
        if (player == null) {
            throw new NullPointerException("'player' is null");
        }

        if (pit == null) {
            throw new NullPointerException("'pit' is null");
        }

        // pit should be player's
        if (!player.getPosition().getPits().contains(pit)) {
            return false;
        }

        // pit should not be big
        if (pit.isBig()) {
            return false;
        }

        // pit should contain stones
        List<Stone> stones = pit.getStones();
        if (stones.isEmpty()) {
            return false;
        }

        return true;
    }

    public static void sow(final Player player, final Pit pit) {
        if (player == null) {
            throw new NullPointerException("'player' is null");
        }

        if (pit == null) {
            throw new NullPointerException("'pit' is null");
        }

        final List<Stone> stones = pit.getStones();
        final int numStones = stones.size();
        pit.emptyPit();
        final Pit playerBigPit = player.getPosition().getBigPit();

        boolean activePlayerGetsAnotherTurn = false;

        // sow into next pits
        Pit currentPit = pit;
        for (int stoneIdx = 0; stoneIdx < numStones; stoneIdx++) {
            final Stone currentStone = stones.get(stoneIdx);
            currentPit = currentPit.getNext();
            // don't put stones in opponent's big currentPit
            if (currentPit.isBig() && !playerBigPit.equals(currentPit)) {
                currentPit = currentPit.getNext();
            }
            // if last stone is put in an empty currentPit owned by
            // current player, capture other player's stones
            if (isLastStone(numStones, stoneIdx)) {
                if (canCapturePit(player, currentPit)) {
                    playerBigPit.addStone(currentStone);
                    capturePit(player, currentPit);
                } else if (currentPit.isBig() && playerBigPit.equals(currentPit)) {
                    activePlayerGetsAnotherTurn = true;
                    currentPit.addStone(currentStone);
                } else {
                    currentPit.addStone(currentStone);
                }
            } else {
                currentPit.addStone(currentStone);
            }
        }
        if (!activePlayerGetsAnotherTurn) {
            AwaleGame.INSTANCE.switchPlayers();
        }
    }

    private static boolean canCapturePit(final Player player, final Pit pit) {
        assert player != null : "'player' is null";
        assert pit != null : "'pit' is null";

        return pit.isEmpty() && !pit.isBig() && player.getPosition().getPits().contains(pit);
    }

    private static void capturePit(final Player player, final Pit pit) {
        assert player != null : "'player' is null";
        assert pit != null : "'pit' is null";

        // put opponent's stones in big pit
        Pit oppositePit = pit.getOpposite();
        List<Stone> opponentsStones = oppositePit.getStones();
        for (Stone opponentsStone : opponentsStones) {
            player.getPosition().getBigPit().addStone(opponentsStone);
        }
        oppositePit.emptyPit();
    }

    /**
     * Determines whether the current stone to be sowed is the last one.
     * @param numStones
     *       The total number of stones to be sowed. Should be {@code numStones >= 0}.
     * @param currentStoneNum
     *       The index of the stone that is to be sowed. Should be {@code 0 <= currentStoneNum < numStones}.
     * @return {@code true} if {@code currentStoneNum} is the last stone to be sowed, {@code false} otherwise.
     */
    private static boolean isLastStone(int numStones, int currentStoneNum) {
        assert numStones != currentStoneNum : "'currentStoneNum' is equal to 'numStones'";
        assert numStones > currentStoneNum : "'currentStoneNum' is equal to or greater than 'numStones'";
        assert numStones >= 0 : "'numStones' is less than 0";
        assert currentStoneNum >= 0 : "'currentStoneNum' is less than 0";

        return numStones - currentStoneNum == 1;
    }

    public static void putAllStonesInBigPit(final Player player) {
        if (player == null) {
            throw new NullPointerException("'player' is null");
        }

        final Pit bigPit = player.getPosition().getBigPit();
        final List<Pit> smallPits = player.getPosition().getPits();
        smallPits.remove(bigPit);
        for (Pit smallPit : smallPits) {
            for (Stone stone : smallPit.getStones()) {
                bigPit.addStone(stone);
            }
            smallPit.emptyPit();
        }
    }
}
