package com.relentlesscoding.awale.services;

import com.relentlesscoding.awale.domain.AwaleGame;
import com.relentlesscoding.awale.domain.Player;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

@Component
public class MyListener {

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @EventListener
    public void handle(SessionConnectedEvent sessionConnectedEvent) {
        final String sessionId = sessionConnectedEvent.getMessage().getHeaders().get("simpSessionId").toString();
        AwaleGameService.connectPlayer(sessionId);
        this.simpMessagingTemplate.convertAndSend("/topic/gameState", AwaleGameService.createGameStateResponse(AwaleGame.INSTANCE));
    }

    @EventListener
    public void handle(SessionDisconnectEvent sessionDisconnectEvent) {
        final String sessionId = sessionDisconnectEvent.getSessionId();
        AwaleGameService.disconnectPlayer(sessionId);
        System.err.println("Session disconnected");
    }
}
