package com.relentlesscoding.awale.services;

import com.relentlesscoding.awale.domain.AwaleGame;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class ScheduleTask {

    @Autowired
    private SimpMessagingTemplate template;

    @Scheduled(fixedRate = 5000)
    public void trigger() {
        this.template.convertAndSend("/topic/gameState", AwaleGameService.createGameStateResponse(AwaleGame.INSTANCE));
    }
}
