package com.relentlesscoding.awale.services;

import com.relentlesscoding.awale.domain.*;

import java.util.List;

public class AwaleGameService {

    /**
     * Validates user input {@code field} and tries to parse an {@link Integer} from it.
     * @param usField The field number provided by the user.
     * @return An {@link Integer} representing the index of a playing field, or {@code -1} if the input was invalid.
     */
    public static Integer validateFieldNumber(final String usField) {
        int fieldNum = -1;
        if (usField == null || usField.isEmpty()) {
            return -1;
        }
        if (!usField.matches("^\\d{1,2}$")) {
            // return some kind of error object
            return -1;
        }
        // number should be between 0 and 13
        try {
            fieldNum = Integer.parseInt(usField);
        } catch (NumberFormatException e) {
            return -1;
        }
        if (fieldNum < 0 || fieldNum > 13) {
            return -1;
        }
        return fieldNum;
    }

    /**
     * Checks if a {@link Player} has no stones left in the pits owned by her.
     * @param player The player whose pits are going to be checked.
     * @return {@code true} if the {@link Player} is out of stones, {@code false} otherwise.
     */
    private static boolean ranOutOfStones(final Player player) {
        assert player != null : "'player' is null";

        List<Pit> pits = player.getPosition().getPits();
        pits.remove(player.getPosition().getBigPit());
        return pits.stream().filter(Pit::isEmpty).count() == pits.size();
    }

    public static void handleVictoryConditions(final Player activePlayer, final Player passivePlayer) {
        if (activePlayer == null) {
            throw new NullPointerException("'activePlayer' is null");
        }
        if (passivePlayer == null) {
            throw new NullPointerException("'passivePlayer' is null");
        }

        // as soon as one of the players runs out of stones, the game is finished
        final boolean isActivePlayerOutOfStones = ranOutOfStones(activePlayer);
        final boolean isPassivePlayerOutOfStones = ranOutOfStones(passivePlayer);
        if (isActivePlayerOutOfStones) {
            // all stones of opponent should be put in his big pit
            PlayerService.putAllStonesInBigPit(passivePlayer);
        } else if (isPassivePlayerOutOfStones) {
            PlayerService.putAllStonesInBigPit(activePlayer);
        }

        if (isActivePlayerOutOfStones || isPassivePlayerOutOfStones) {
            AwaleGame.INSTANCE.becomeFinished();
        }
    }

    public static AwaleGameState createGameStateResponse(final AwaleGame awaleGame, final String error) {
        if (awaleGame == null) {
            throw new NullPointerException("'awaleGame' is null");
        }
        if (error == null) {
            throw new NullPointerException("'error' is null");
        }

        return new AwaleGameState(awaleGame.getBoard().getPits(),
                                  awaleGame.getNorthPlayer(),
                                  awaleGame.getSouthPlayer(),
                                  awaleGame.getActivePlayer(),
                                  awaleGame.getPassivePlayer(),
                                  awaleGame.isFinished(),
                                  error);
    }

    public static AwaleGameState createGameStateResponse(final AwaleGame awaleGame) {
        return createGameStateResponse(awaleGame, "");
    }

    /**
     * Reset game to initial state.
     */
    public static void reset() {
        Board.INSTANCE.reset();
        AwaleGame.INSTANCE.reset();
        PlayerPosition.NORTH.reset();
        PlayerPosition.SOUTH.reset();
    }

    public static void connectPlayer(final String sessionId) {
        final AwaleGame game = AwaleGame.INSTANCE;
        final Player southPlayer = game.getSouthPlayer();
        final Player northPlayer = game.getNorthPlayer();
        if (!southPlayer.isConnected()) {
            southPlayer.setConnected(true);
            southPlayer.setSessionId(sessionId);
        } else if (!northPlayer.isConnected()) {
            northPlayer.setConnected(true);
            northPlayer.setSessionId(sessionId);
        }
    }

    public static void disconnectPlayer(final String sessionId) {
        if (sessionId == null || sessionId.isEmpty()) {
            throw new IllegalArgumentException("'sessionId' is null or empty");
        }

        final AwaleGame game = AwaleGame.INSTANCE;
        final Player southPlayer = game.getSouthPlayer();
        final Player northPlayer = game.getNorthPlayer();
        if (sessionId.equals(southPlayer.getSessionId())) {
            southPlayer.setConnected(false);
            southPlayer.setSessionId("");
        } else if (sessionId.equals(northPlayer.getSessionId())) {
            northPlayer.setConnected(false);
            northPlayer.setSessionId("");
        } else {
            // what happened?
            System.err.println("Unknown sessionId '" + sessionId + "' found.");
        }
    }
}
