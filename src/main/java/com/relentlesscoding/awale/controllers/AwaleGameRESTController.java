package com.relentlesscoding.awale.controllers;

import com.relentlesscoding.awale.domain.*;
import com.relentlesscoding.awale.services.AwaleGameService;
import com.relentlesscoding.awale.services.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

/**
 * Read more about REST controllers. How should this be called? What is best practice.
 */
@Controller
public class AwaleGameRESTController {

    @Autowired
    private AwaleGameService awaleGameService;

    /**
     * Performs a single turn for the current player and updates the game state.
     * @param fieldMessage The field number provided by the user.
     * @return The new game state after the player's turn.
     */
    @MessageMapping("/makeMove")
    @SendTo("/topic/gameState")
    public AwaleGameState makeMove(final FieldMessage fieldMessage) {
        if (!AwaleGame.INSTANCE.isFinished()) {
            final int fieldNum = awaleGameService.validateFieldNumber(fieldMessage.getField());
            final Player activePlayer = AwaleGame.INSTANCE.getActivePlayer();
            final Player passivePlayer = AwaleGame.INSTANCE.getPassivePlayer();
            final Pit pit = Board.INSTANCE.getPits().get(fieldNum);
            if (PlayerService.isValidMove(activePlayer, pit)) {
                PlayerService.sow(activePlayer, pit);
            } else {
                // return some kind of error object
                final String error = "INVALID_MOVE";
                return awaleGameService.createGameStateResponse(AwaleGame.INSTANCE, error);
            }

            awaleGameService.handleVictoryConditions(activePlayer, passivePlayer);
        }
        return awaleGameService.createGameStateResponse(AwaleGame.INSTANCE);
    }

    /**
     * Resets game to initial state.
     * @return An object informing the caller whether the state was successfully reset or not.
     */
    @MessageMapping("/reset")
    @SendTo("/topic/gameState")
    public AwaleGameState reset() {
        AwaleGameService.reset();
        return AwaleGameService.createGameStateResponse(AwaleGame.INSTANCE);
    }
}
