package com.relentlesscoding.awale.controllers;

import com.google.common.collect.Lists;
import com.relentlesscoding.awale.domain.AwaleGame;
import com.relentlesscoding.awale.domain.Pit;
import com.relentlesscoding.awale.domain.PlayerPosition;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.stream.Collectors;

@Controller
public class AwaleGameController {

    @RequestMapping("/awale")
    public String awale(final Model model) {
        model.addAttribute("isGameFinished", AwaleGame.INSTANCE.isFinished());
        model.addAttribute("pits", AwaleGame.INSTANCE.getBoard().getPits());
        model.addAttribute("northPlayerPits", Lists.reverse(PlayerPosition.NORTH.getPits().stream().filter(Pit::isSmall).collect(Collectors.toList())));
        model.addAttribute("southPlayerPits", PlayerPosition.SOUTH.getPits().stream().filter(Pit::isSmall).collect(Collectors.toList()));
        model.addAttribute("activePlayer", AwaleGame.INSTANCE.getActivePlayer());
        model.addAttribute("northPlayer", AwaleGame.INSTANCE.getNorthPlayer());
        model.addAttribute("southPlayer", AwaleGame.INSTANCE.getSouthPlayer());
        return "awale";
    }
}