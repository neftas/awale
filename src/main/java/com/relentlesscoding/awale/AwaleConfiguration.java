package com.relentlesscoding.awale;

import com.relentlesscoding.awale.services.AwaleGameService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AwaleConfiguration {

    @Bean
    public AwaleGameService awaleGameService() {
        return new AwaleGameService();
    }
}
