package com.relentlesscoding.awale.tests;

import com.google.common.collect.ImmutableList;
import com.relentlesscoding.awale.services.AwaleGameService;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class AwaleGameServiceTest {

    @Test
    public void validateFieldNumberWithInputNullShouldReturnMinus1() {
        assertEquals(new Integer(-1), AwaleGameService.validateFieldNumber(null));
    }

    @Test
    public void validateFieldNumberWithEmptyStringShouldReturnIllegalArgumentException() {
        assertEquals(new Integer(-1), AwaleGameService.validateFieldNumber(""));
    }

    @Test
    public void validateFieldNumberWithLettersShouldReturnMinus1() {
        assertEquals(new Integer(-1), AwaleGameService.validateFieldNumber("abc"));
    }

    @Test
    public void validateFieldNumberWithInvalidInputShouldReturnMinus1() {
        final List<String> inputs = ImmutableList.of("123abc", "1a", "abc123", "-10");

        for (String input : inputs) {
            assertEquals(new Integer(-1), AwaleGameService.validateFieldNumber(input));
        }
    }
}
