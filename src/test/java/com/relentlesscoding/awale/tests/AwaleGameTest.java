package com.relentlesscoding.awale.tests;

import com.relentlesscoding.awale.domain.AwaleGame;
import com.relentlesscoding.awale.domain.Player;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class AwaleGameTest {

    @Test
    public void playersShouldSwitch() {
        // arrange
        final Player southPlayer = AwaleGame.INSTANCE.getSouthPlayer();
        final Player northPlayer = AwaleGame.INSTANCE.getNorthPlayer();
        // assert
        assertNotEquals(southPlayer, northPlayer);
        assertEquals(southPlayer, AwaleGame.INSTANCE.getActivePlayer());
        assertEquals(northPlayer, AwaleGame.INSTANCE.getPassivePlayer());
        // act
        AwaleGame.INSTANCE.switchPlayers();
        // assert
        assertEquals(northPlayer, AwaleGame.INSTANCE.getActivePlayer());
        assertEquals(southPlayer, AwaleGame.INSTANCE.getPassivePlayer());
    }
}
