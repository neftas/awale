package com.relentlesscoding.awale.tests;

import com.relentlesscoding.awale.domain.Board;
import com.relentlesscoding.awale.domain.Pit;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class BoardTest {
    private List<Pit> pits;

    @Before
    public void setup() {
        pits = Board.INSTANCE.getPits();
    }

    @After
    public void tearDown() {
        pits = null;
    }

    @Test
    public void boardShouldContainExactly14Pits() {
        assertEquals(14, pits.size());
    }

    @Test
    public void boardShouldContainExactly2BigPits() {
        // arrange
        int expected = 2;
        // act
        long actual = pits.stream().filter(Pit::isBig).count();
        // assert
        assertEquals(expected, actual);
    }

    @Test
    public void pitsShouldAllHaveDifferentIds() {
        for (int i = 0, j = pits.size(); i < j; i++) {
            for (int k = 0, l = pits.size(); k < l; k++) {
                if (i == k) {
                    assertEquals(pits.get(i), pits.get(k));
                } else {
                    assertNotEquals(pits.get(i), pits.get(k));
                }
            }
        }
    }

    @Test
    public void bigPitsShouldBeAtPosition6And13() {
        assertTrue(pits.get(6).isBig());
        assertTrue(pits.get(13).isBig());
    }

    @Test
    public void allPitsShouldBeSmallExceptFor6And13() {
        for (int i = 0, j = pits.size(); i < j; i++) {
            if (i == 6 || i == 13) {
                continue;
            }
            assertFalse(pits.get(i).isBig());
        }
    }

    @Ignore
    @Test
    public void listOfPitsShouldBeImmutable() {
        pits.clear();
        assertNotSame(pits, Board.INSTANCE.getPits());
    }

    @Test
    public void oppositePitsShouldBeProperlyLinked() {
        int idxSouth[] = {0, 1, 2, 3, 4, 5, 6};
        int idxNorth[] = {12, 11, 10, 9, 8, 7, 13};
        for (int idx = 0, len = idxSouth.length; idx < len; idx++) {
            assertEquals(pits.get(idxSouth[idx]).getOpposite(), pits.get(idxNorth[idx]));
        }
    }
}
