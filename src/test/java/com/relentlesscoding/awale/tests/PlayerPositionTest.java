package com.relentlesscoding.awale.tests;

import com.relentlesscoding.awale.domain.Board;
import com.relentlesscoding.awale.domain.Pit;
import com.relentlesscoding.awale.domain.PlayerPosition;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

public class PlayerPositionTest {
    private List<Pit> northPits;
    private List<Pit> southPits;

    @Before
    public void setup() {
        northPits = PlayerPosition.NORTH.getPits();
        southPits = PlayerPosition.SOUTH.getPits();
    }

    @After
    public void tearDown() {
        northPits = null;
        southPits = null;
    }

    @Test
    public void northAndSouthShouldEachHave7Pits() {
        assertEquals(7, northPits.size());
        assertEquals(7, southPits.size());
    }

    @Test
    public void northAndSouthShouldEachHaveOneBigPit() {
        assertEquals(1, northPits.stream().filter(Pit::isBig).count());
        assertEquals(1, southPits.stream().filter(Pit::isBig).count());
    }

    @Test
    public void northAndSouthShouldHaveBigPitAsLastElement() {
        assertTrue("last element in northPit is not big pit", northPits.get(northPits.size() - 1).isBig());
        assertTrue("last element in northPit is not big pit", southPits.get(northPits.size() - 1).isBig());
    }

    @Test
    public void northAndSouthListOfPitsShouldNotContainSamePits() {
        for (Pit northPit : northPits) {
            for (Pit southPit : southPits) {
                assertNotEquals(northPit, southPit);
            }
        }
    }

    @Test
    public void listOfPitsShouldBeImmutable() {
        northPits.clear();
        assertNotEquals(northPits.size(), PlayerPosition.NORTH.getPits().size());
        southPits.clear();
        assertNotEquals(northPits.size(), PlayerPosition.SOUTH.getPits().size());
    }

    @Test
    public void pitsShouldBeAssignedToCorrectPlayers() {
        List<Pit> allPits = Board.INSTANCE.getPits();
        // first seven pits should be assigned to southPlayer
        for (int i = 0, j = 7; i < j; i++) {
            assertTrue(southPits.contains(allPits.get(i)));
        }
        // last seven pits should be assigned to northPlayer
        for (int i = 7, j = 14; i < j; i++) {
            assertTrue(northPits.contains(allPits.get(i)));
        }
    }
}
