package com.relentlesscoding.awale.tests;

import com.relentlesscoding.awale.domain.*;
import com.relentlesscoding.awale.services.AwaleGameService;
import com.relentlesscoding.awale.services.PlayerService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PlayerServiceTest {
    private PlayerService playerService;
    private Player southPlayer;
    private Player northPlayer;
    private List<Pit> pits;

    @Before
    public void setup() {
        this.playerService = new PlayerService();
        this.southPlayer = Player.newInstance("abc", PlayerPosition.SOUTH);
        this.northPlayer = Player.newInstance("def", PlayerPosition.NORTH);
        this.pits = Board.INSTANCE.getPits();
    }

    @After
    public void tearDown() {
        playerService = null;
        southPlayer = null;
        northPlayer = null;
        for (Pit pit : pits) {
            pit.emptyPit();
            if (!pit.isBig()) {
                for (int i = 0; i < Pit.NUM_INITIAL_STONES; i++) {
                    pit.addStone(Stone.newInstance());
                }
            }
        }
    }

    @Test(expected = NullPointerException.class)
    public void isValidMoveWithNullPlayerShouldThrowIllegalArgumentException() {
        playerService.isValidMove(null, pits.get(0));
    }

    @Test(expected = NullPointerException.class)
    public void isValidMoveWithNullPitShouldThrowIllegalArgumentException() {
        playerService.isValidMove(southPlayer, null);
    }

    @Test(expected = NullPointerException.class)
    public void isValidMoveWithNullArgumentsShouldThrowIllegalArgumentException() {
        playerService.isValidMove(null, null);
    }

    @Test
    public void playerCanPlayOwnPitWithStonesInIt() {
        // arrange
        Stone stone = Stone.newInstance();
        Pit pit = pits.get(0);
        pit.setStones(new ArrayList<>(Arrays.asList(stone)));
        // act & assert
        assertTrue(playerService.isValidMove(southPlayer, pit));
    }

    @Test
    public void playerCannotPlayOpponentsPit() {
        // arrange
        Stone stone = Stone.newInstance();
        // 7 is northPlayer's pit
        Pit pit = pits.get(7);
        pit.setStones(new ArrayList<>(Arrays.asList(stone)));
        // act & assert
        assertFalse(playerService.isValidMove(southPlayer, pit));
    }

    @Test
    public void playerCannotPlayEitherBigPit() {
        // arrange
        Stone stone = Stone.newInstance();
        // 6 is southPlayer's big pit
        Pit pit = pits.get(6);
        pit.setStones(new ArrayList<>(Arrays.asList(stone)));
        // act & assert
        assertFalse(playerService.isValidMove(southPlayer, pit));
        assertFalse(playerService.isValidMove(northPlayer, pit));

        // arrange
        stone = Stone.newInstance();
        // 13 is northPlayer's big pit
        pit = pits.get(13);
        pit.setStones(new ArrayList<>(Arrays.asList(stone)));
        // act & assert
        assertFalse(playerService.isValidMove(southPlayer, pit));
        assertFalse(playerService.isValidMove(northPlayer, pit));
    }

    @Test
    public void playerCannotPlayEmptyPit() {
        // arrange: create empty pit
        Pit pit = pits.get(0);
        pit.emptyPit();
        // act & assert
        assertFalse(playerService.isValidMove(southPlayer, pit));
    }

    @Test(expected = NullPointerException.class)
    public void sowWithNullArgumentsThrowsNullPointerException() {
        playerService.sow(null, null);
    }

    @Test
    public void sowSouthPlayerFirstPitInitiallyShouldEmptyFirstPit() {
        // arrange
        Pit firstPit = pits.get(0);
        // act
        playerService.sow(southPlayer, firstPit);
        // assert
        assertTrue(Board.INSTANCE.getPits().get(0).isEmpty());
    }

    @Test
    public void playingFirstPitShouldResultInAllOtherPitsGettingExtraStone() {
        // arrange
        Pit firstPit = pits.get(0);
        // act
        playerService.sow(southPlayer, firstPit);
        // assert
        List<Pit> pitsAfterSow = Board.INSTANCE.getPits();
        assertEquals(7, pitsAfterSow.get(1).count());
        assertEquals(7, pitsAfterSow.get(2).count());
        assertEquals(7, pitsAfterSow.get(3).count());
        assertEquals(7, pitsAfterSow.get(4).count());
        assertEquals(7, pitsAfterSow.get(5).count());
        // big pit
        assertEquals(1, pitsAfterSow.get(6).count());

        assertEquals("South player should have a score of 1 after his turn",
                     1,
                     southPlayer.getScore());
    }

    @Test
    public void playingSixthPitShouldResultInAllOtherPitsGettingExtraStone() {
        // arrange
        Pit sixthPit = pits.get(5);
        // act
        playerService.sow(southPlayer, sixthPit);
        // assert
        List<Pit> pitsAfterSow = Board.INSTANCE.getPits();
        // big pit
        assertEquals(1, pitsAfterSow.get(6).count());
        // opponent's pits
        assertEquals(7, pitsAfterSow.get(7).count());
        assertEquals(7, pitsAfterSow.get(8).count());
        assertEquals(7, pitsAfterSow.get(9).count());
        assertEquals(7, pitsAfterSow.get(10).count());
        assertEquals(7, pitsAfterSow.get(11).count());

        assertEquals("South player should have a score of 1 after his turn",
                     1,
                     southPlayer.getScore());
    }

    @Test
    public void playingTwelfthPitShouldResultInAllOtherPitsGettingExtraStone() {
        // arrange
        Pit twelfthPit = pits.get(12);
        // act
        playerService.sow(northPlayer, twelfthPit);
        // assert
        List<Pit> pitsAfterSow = Board.INSTANCE.getPits();
        // big pit
        assertEquals(1, pitsAfterSow.get(13).count());
        // opponent's pits
        assertEquals(7, pitsAfterSow.get(0).count());
        assertEquals(7, pitsAfterSow.get(1).count());
        assertEquals(7, pitsAfterSow.get(2).count());
        assertEquals(7, pitsAfterSow.get(3).count());
        assertEquals(7, pitsAfterSow.get(4).count());

        assertEquals("North player should have a score of 1 after his turn",
                     1,
                     northPlayer.getScore());
    }

    @Test
    public void capturingShouldEmptyTwoPits() {
        final Pit firstPit = this.pits.get(0);
        playerService.sow(southPlayer, firstPit);
        assertEquals(1, southPlayer.getScore());
        /*
         *   | 6 | 6 | 6 | 6 | 6 | 6 |
         * 0 |---+---+---+---+---+---| 1
         *   | 0 | 7 | 7 | 7 | 7 | 7 |
         */
        this.pits = Board.INSTANCE.getPits();
        assertTrue(pits.get(0).isEmpty());
        int expected[] = {0, 7, 7, 7, 7, 7, 1, 6, 6, 6, 6, 6, 6, 0};
        for (int i = 0, j = expected.length; i < j; i++) {
            assertEquals(expected[i], this.pits.get(i).count());
        }

        final Pit secondPit = this.pits.get(1);
        assertEquals("Second pit should contain 7 stones", 7, secondPit.count());
        playerService.sow(southPlayer, secondPit);
        assertEquals(2, southPlayer.getScore());
        /*
         *   | 6 | 6 | 6 | 6 | 7 | 7 |
         * 0 |---+---+---+---+---+---| 2
         *   | 0 | 0 | 8 | 8 | 8 | 8 |
         */

        this.pits = Board.INSTANCE.getPits();
        assertTrue(this.pits.get(1).isEmpty());
        expected = new int[] {0, 0, 8, 8, 8, 8, 2, 7, 7, 6, 6, 6, 6, 0};
        for (int i = 0, j = expected.length; i < j; i++) {
            assertEquals(expected[i], this.pits.get(i).count());
        }

        final Pit eightPit = this.pits.get(7);
        playerService.sow(northPlayer, eightPit);
        /*
         *   | 7 | 7 | 7 | 7 | 8 | 0 |
         * 1 |---+---+---+---+---+---| 2
         *   | 1 | 0 | 8 | 8 | 8 | 8 |
         */
        this.pits = Board.INSTANCE.getPits();
        expected = new int[] {1, 0, 8, 8, 8, 8, 2, 0, 8, 7, 7, 7, 7, 1};
        for (int i = 0, j = expected.length; i < j; i++) {
            assertEquals(expected[i], this.pits.get(i).count());
        }

        playerService.sow(southPlayer, firstPit);
        /*
         *   | 7 | 0 | 7 | 7 | 8 | 0 |
         * 1 |---+---+---+---+---+---| 10
         *   | 0 | 0 | 8 | 8 | 8 | 8 |
         */
        assertEquals(10, southPlayer.getScore());
        assertEquals(1, northPlayer.getScore());
        expected = new int[] {0, 0, 8, 8, 8, 8, 10, 0, 8, 7, 7, 0, 7, 1};
        for (int i = 0, j = expected.length; i < j; i++) {
            assertEquals(expected[i], this.pits.get(i).count());
        }
    }

    // don't put stones in opponent's big pit
    @Test
    public void doNotPutStonesInOpponentsBigPit() {
        final Pit sixthPit = this.pits.get(5);
        List<Stone> stones = new ArrayList<>(10);
        for (int i = 0; i < 10; i++) {
            stones.add(Stone.newInstance());
        }
        sixthPit.setStones(stones);
        playerService.sow(southPlayer, sixthPit);
        assertEquals(1, southPlayer.getScore());
        assertEquals(0, northPlayer.getScore());
        /*
         *   | 7 | 7 | 7 | 7 | 7 | 7 |
         * 0 |---+---+---+---+---+---| 1
         *   | 7 | 7 | 7 | 6 | 6 | 0 |
         */
        int expected[] = {7, 7, 7, 6, 6, 0, 1, 7, 7, 7, 7, 7, 7, 0};
        for (int i = 0, j = expected.length; i < j; i++) {
            assertEquals(expected[i], this.pits.get(i).count());
        }
    }

    @Test(expected = NullPointerException.class)
    public void putAllStonesInBigPitWithNullArgumentShouldThrowNullPointerException() {
        PlayerService.putAllStonesInBigPit(null);
    }

    @Test
    public void putAllStonesInBigPitInitiallyShouldPut36StonesInBigPit() {
        // act
        PlayerService.putAllStonesInBigPit(southPlayer);
        // assert
        assertEquals(36, southPlayer.getScore());
    }

    @Test
    public void onePlayerOutOfStonesShouldFinishGame() {
        // act
        PlayerService.putAllStonesInBigPit(southPlayer);
        AwaleGameService.handleVictoryConditions(southPlayer, northPlayer);
        assertTrue(AwaleGame.INSTANCE.isFinished());
    }

}
