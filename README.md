# awale
A browser game based on [oware / awale / aware](https://en.wikipedia.org/wiki/Oware).

## Disclaimer

This is very much a work in progress. The idea is to be able to play this game
against an opponent over the internet. Currently, however, only the "hotseat"
version is build.

## How to build

    ./gradlew build
    java -jar build/libs/awale.jar
